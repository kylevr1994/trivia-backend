### Prerequisites

Before you begin, ensure you have met the following requirements:

Java installed. This application requires Java **21.0.1**
Maven installed. This application is built using Maven **apache-maven-3.9.5**

### Getting Started

To run the application, follow these steps:
1. Clone the repository
2. Navigate to the project directory
3. Build the project using "**mvn clean install**"
4. Run the project using "**mvn spring-boot:run**"
