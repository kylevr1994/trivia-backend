package com.kyle.trivia;

import java.util.List;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.kyle.trivia.model.ClientAnswer;
import com.kyle.trivia.model.TriviaQuestion;
import com.kyle.trivia.model.TriviaQuestionHidden;
import com.kyle.trivia.model.TriviaResponse;

import java.util.ArrayList;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;


@RestController
@SpringBootApplication
@RequiredArgsConstructor
public class TriviaApplication {

	private final RestTemplate restTemplate;
	private List<TriviaQuestion> triviaQuestions = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(TriviaApplication.class, args);
	}
	

	/**
	 * Provide 5 trivia questions from the Trivia API. The correct answers are saved in
	 * the backend and stript from the data given to the cliënt.
	 * @return List<TrivaQuestionHidden> a.k.a 5 questions without the answers
	 */
	@GetMapping(value = "/questions", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<TriviaQuestionHidden> getQuestions() {
		try{
			// Get opentdb API questions
			String apiUrl = "https://opentdb.com/api.php?amount=5";
			TriviaResponse triviaResponse = restTemplate.getForObject(apiUrl, TriviaResponse.class);

			// Store the correct answers on the server side
			triviaQuestions = triviaResponse.getResults();

			// Combine incorrect and correct awnsers to send to the client side + add id's
			List<TriviaQuestionHidden> questionsToSend = new ArrayList<>();
			for(int i = 0; i < triviaQuestions.size(); i++){
				TriviaQuestion question = triviaQuestions.get(i);
				question.setId(i + 1);
				TriviaQuestionHidden hiddenQuestion = new TriviaQuestionHidden(question);
				questionsToSend.add(hiddenQuestion);
			}

			return questionsToSend;

		} catch(Exception ex){
			System.out.println(ex); // Note: for this project I will only log the exception.
			return null;
		}
	}


	/**
	 * Check if the given answers are correct by comparing it to the TriviaQuestion list.
	 * When the correct answer is given this method sets isCorrect to true, if not to false.
	 * The time complexity of this method is O(N) where N is the number of elements in triviaQuestions.
	 * @return List<ClientAnswers> with isCorrect value added.
	 */
	@PostMapping(value = "/checkanswers", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<ClientAnswer> checkAnswers(@RequestBody List<ClientAnswer> clientAnswers) {
		try {
			// Convert triviaQuestions to a Map for efficient lookup
			Map<Integer, TriviaQuestion> questionMap = triviaQuestions.stream()
					.collect(Collectors.toMap(TriviaQuestion::getId, q -> q));

			for (ClientAnswer clientAnswer : clientAnswers) {
				TriviaQuestion question = questionMap.get(clientAnswer.getId());
				if (question != null && clientAnswer.getAnswer() != null) {
					if (clientAnswer.getAnswer().equals(question.getCorrect_answer())) {
						clientAnswer.setIsCorrect(true);
					} else {
						clientAnswer.setIsCorrect(false);
					}
				}
			}
			return clientAnswers;
		} catch(Exception ex){
			System.out.println(ex); // Note: for this project I will only log the exception.
			return null;
		}
	}

	public void setTriviaQuestions(List<TriviaQuestion> questions) {
		this.triviaQuestions = questions;
	}

}
