package com.kyle.trivia.model;

public class ClientAnswer {
    
    private int id;
    private String answer;
    private Boolean isCorrect = false;
    
    // Gettters and Setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getAnswer() {
        return answer;
    }
    public void setAnswer(String awnser) {
        this.answer = awnser;
    }
    public Boolean getIsCorrect() {
        return isCorrect;
    }
    public void setIsCorrect(Boolean isCorrect) {
        this.isCorrect = isCorrect;
    }
    
}
