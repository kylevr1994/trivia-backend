package com.kyle.trivia.model;

import java.util.List;

public class TriviaQuestionHidden {
    
    private int id;
    private String category;
    private String type;
    private String difficulty;
    private String question;
    private List<String> answers;
    
    
    // Constructors
    public TriviaQuestionHidden() {
    }

    public TriviaQuestionHidden(TriviaQuestion question) {
        this.id = question.getId();
        this.category = question.getCategory();
        this.type = question.getType();
        this.difficulty = question.getDifficulty();
        this.question = question.getQuestion();
        this.answers = question.getAnswers();
    }


    // Getter and Setters
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }   
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public String getDifficulty() {
        return difficulty;
    }
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }
    public String getQuestion() {
        return question;
    }
    public void setQuestion(String question) {
        this.question = question;
    }
    public List<String> getAnswers() {
        return answers;
    }
    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }
}
