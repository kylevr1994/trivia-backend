package com.kyle.trivia;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestTemplate;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kyle.trivia.model.ClientAnswer;
import com.kyle.trivia.model.TriviaQuestion;
import com.kyle.trivia.model.TriviaResponse;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import java.util.ArrayList;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
class TriviaApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RestTemplate restTemplate;

    @Autowired
    private TriviaApplication triviaApplication;

    @Test
    void testPostCheckAnswers() throws Exception {
        // Mock the trivia questions
        List<TriviaQuestion> sampleQuestions = createSampleQuestions();
    
        // Set up the TriviaApplication instance with the mocked questions
        triviaApplication.setTriviaQuestions(sampleQuestions);

        // Create clientAnswer data
        List<ClientAnswer> clientAnswers = createClientAnswers();

        // Perform the POST request to /checkanswers
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/checkanswers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new ObjectMapper().writeValueAsString(clientAnswers)))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
    

        MvcResult mvcResult = result.andReturn();
        String responseBody = mvcResult.getResponse().getContentAsString();
        List<ClientAnswer> responseClientAnswers = new ObjectMapper().readValue(responseBody, new TypeReference<List<ClientAnswer>>() {});
    
        // Assertions
        assertEquals(clientAnswers.size(), responseClientAnswers.size());
    
        for (int i = 0; i < clientAnswers.size(); i++) {
            ClientAnswer originalAnswer = clientAnswers.get(i);
            ClientAnswer responseAnswer = responseClientAnswers.get(i);
    
            assertEquals(originalAnswer.getId(), responseAnswer.getId());
            assertEquals(originalAnswer.getAnswer(), responseAnswer.getAnswer());
        }
    }


    @Test
    void testGetQuestions() throws Exception {

        // Mock the external API response
        List<TriviaQuestion> sampleQuestions = createSampleQuestions();
        TriviaResponse mockResponse = new TriviaResponse();
        mockResponse.setResults(sampleQuestions);
        mockResponse.setResponse_code(200);

        // Return mockResponse instead of API call
        Mockito.when(restTemplate.getForObject(Mockito.anyString(), Mockito.eq(TriviaResponse.class)))
            .thenReturn(mockResponse);
           
        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.get("/questions"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(3));

        result.andReturn();
    }

    private List<ClientAnswer> createClientAnswers() {
       
        // correct
        ClientAnswer answer = new ClientAnswer();
        answer.setId(1);
        answer.setAnswer("Akali");

        // incorrect
        ClientAnswer answer2 = new ClientAnswer();
        answer2.setId(2);
        answer2.setAnswer("Stark");

        // correct
        ClientAnswer answer3 = new ClientAnswer();
        answer2.setId(3);
        answer2.setAnswer("The Witcher 3");

        List<ClientAnswer> clientAnswers = new ArrayList<>(List.of(
            answer,
            answer2,
            answer3
        ));

        return clientAnswers;
    }

    // Helper method to create sample questions
    private List<TriviaQuestion> createSampleQuestions() {

        // Question 1
        TriviaQuestion question1 = new TriviaQuestion();
        question1.setId(1);
        question1.setCategory("Gaming");
        question1.setQuestion("What is Kyle's most played champion in League?");
        question1.setCorrect_answer("Akali");
        question1.setIncorrect_answers(new ArrayList<>(List.of("Xerath", "Zed", "Mundo")));
        question1.setDifficulty("Easy");
        question1.setType("Multiple");

        // Question 2
        TriviaQuestion question2 = new TriviaQuestion();
        question2.setId(2);
        question2.setCategory("Random");
        question2.setQuestion("What is kyle's last name?");
        question2.setCorrect_answer("van Raaij");
        question2.setIncorrect_answers(new ArrayList<>(List.of("Stark", "Parker", "Depp")));
        question2.setDifficulty("Easy");
        question2.setType("Multiple");

        // Question 3
        TriviaQuestion question3 = new TriviaQuestion();
        question3.setId(3);
        question3.setCategory("Gaming");
        question3.setQuestion("What is Kyle's favorite game?");
        question3.setCorrect_answer("The Witcher 3");
        question3.setIncorrect_answers(new ArrayList<>(List.of("The last of us", "cyberpunk 2077", "tetris")));
        question3.setDifficulty("Easy");
        question3.setType("Multiple");

        // Add questions
        List<TriviaQuestion> sampleQuestions = new ArrayList<>();
        sampleQuestions.add(question1);
        sampleQuestions.add(question2);
        sampleQuestions.add(question3);

        return sampleQuestions;
    }
}
